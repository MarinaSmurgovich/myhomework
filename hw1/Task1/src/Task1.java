/**
 * Задание № 1
 * <p>
 * 1) Написать алгоритмы конвертации физических величин.
 * Код должен быть написан в соответствующих функциях
 * вместо комментария со вловом TODO +
 * <p>
 * 2) Написать 2-3 собственные функции по конвертированию величин.
 * Результат вывести в консоль с помощью команды System.out.println().
 * См. Пример ее использования в методе main +
 * <p>
 * PS Предусмотреть деление на 0, в этом случае нужно возвращать число -999. ?
 * <p>
 * Метод проверки: когда запустите метод main и посмотреть результаты в консоли. +
 */

public class Task1 {

    public static double meterToInch(double meter) {
        return meter * 39.3701; // TODO метр -> дюйм
    }

    public static double inchToMeter(double inch) {
        return inch / 39.3701; // TODO дюйм -> метр
    }

    public static double kilogramToGram(double kilogram) {
        return kilogram * 1000; // TODO килограмм -> грамм
    }

    public static double gramToKilogram(double gram) {
        return gram / 1000; // TODO грамм -> килограмм
    }

    public static double literToCubicMeter(double liter) {
        return liter / 1000; // TODO литр -> кубический метр
    }

    public static double cubicMeterToLiter(double cubicMeter) {
        return cubicMeter * 1000; // TODO кубический метр -> литр
    }

    public static double GaToKvMetr(double Ga) {
        return Ga * 100; // TODO гиктар -> квадратный метр
    }

    public static double KvMetrToGa(double KvMetr) {
        return KvMetr / 100; // TODO квадратный метр -> гиктар
    }

    public static double HourToSec(double Hour) {
        return Hour * 3600; // TODO час -> секунды
    }

    public static double SecToHour(double Sec) {
        return Sec / 3600; // TODO секунды -> часы
    }

    public static double GrCtoF(double GrC) {
        return (GrC * 1.8) + 32; // TODO градус Цельсия -> Градус Фаренгейта
    }

    public static double FtoGrC(double F) {
        return (F - 32) / 1.8; // TODO Градус Фаренгейтаградус -> Цельсия
    }

    public static void main(String[] args) {
        double inch = meterToInch(5);
        System.out.println("meterToInch - 5: " + inch);

        inch = meterToInch(0);
        System.out.println("meterToInch - 0: " + inch);

        double meter = inchToMeter(10);
        System.out.println("inchToMeter - 10: " + meter);

        meter = inchToMeter(0);
        System.out.println("inchToMeter - 0: " + meter);

        double gram = kilogramToGram(13);
        System.out.println("kilogramToGram - 13: " + gram);

        gram = kilogramToGram(0);
        System.out.println("kilogramToGram - 0: " + gram);

        double kilogram = gramToKilogram(991);
        System.out.println("gramToKilogram - 991: " + kilogram);

        kilogram = gramToKilogram(0);
        System.out.println("gramToKilogram - 0: " + kilogram);

        double cubicMeter = literToCubicMeter(88);
        System.out.println("literToCubicMeter - 88: " + cubicMeter);

        cubicMeter = literToCubicMeter(0);
        System.out.println("literToCubicMeter - 0: " + cubicMeter);

        double liter = cubicMeterToLiter(69);
        System.out.println("cubicMeterToLiter - 69: " + liter);

        liter = cubicMeterToLiter(0);
        System.out.println("cubicMeterToLiter - 0: " + liter);

        double KvMetr = GaToKvMetr(69);
        System.out.println("GaToKvMetr - 69: " + KvMetr);

        KvMetr = GaToKvMetr(0);
        System.out.println("GaToKvMetr - 0: " + KvMetr);

        double Ga = KvMetrToGa(25);
        System.out.println("KvMetrToGa - 25: " + Ga);

        Ga = KvMetrToGa(0);
        System.out.println("KvMetrToGa - 0 " + Ga);

        double Sec = HourToSec(2);
        System.out.println("HourToSec - 2: " + Sec);

        Sec = HourToSec(0);
        System.out.println("HourToSec - 2: " + Sec);

        double Hour = SecToHour(556785);
        System.out.println("SecToHour - 556785: " + Hour);

        Hour = SecToHour(0);
        System.out.println("SecToHour - 0: " + Hour);

        double F = GrCtoF(56);
        System.out.println("GrCtoF - 56: " + F);

        F = GrCtoF(0);
        System.out.println("GrCtoF - 0: " + F);

        double GrC = FtoGrC(8796);
        System.out.println("FtoGrC - 8796: " + GrC);

        GrC = FtoGrC(0);
        System.out.println("FtoGrC - 0: " + GrC);
    }
}
