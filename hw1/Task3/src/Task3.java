/**
 * Задание № 3 +++
 * <p>
 * 1) Написать алгоритм проверки необходимости человеку работать.
 * Функция имеет два параметра:
 * - isWeekDay показывает это рабочий день (true) или выходной(false)
 * - isHoliday показывает нахожусь ли я в отпуске true) или нет(false)
 * <p>
 * Задание:
 * - если человек в отпуске или сейчас выходной день, то вывести в консоль
 * сообщение "Отдыхай"
 * - иначе вывести сообщение "Давай работай"
 * Вывод сообщения произвести в консоль
 */

public class Task3 {

    public static void checkHoliday(boolean isWeekDay, boolean isHoliday) {
        /**
         * NOTE! Так как isWeekDay и isHoliday булевы переменные,
         * то они могут напрямую использовать я логических операциях.
         * То есть нет необходимости их сравнивать с true или false.
         * Можно было написать вот так:
         *
         * if (!isWeekDay || isHoliday)
         */
        if (!isWeekDay || isHoliday)
            System.out.println("Отдыхай");
        else
            System.out.println("Давай работай");
    }

    public static void main(String[] args) {
        checkHoliday(false, false);
    }
}
