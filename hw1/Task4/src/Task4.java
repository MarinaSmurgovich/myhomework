/**
 * Задание № 4
 *
 * ++++ 1) В переменной min лежит число от 0 до 59.
 * Определите в какую четверть часа попадает это число
 * (в первую, вторую, третью или четвертую).
 *
 * - от 0 до 14 - первая
 * - от 15 до 29 - вторая
 * - от 30 до 44 - третья
 * - от 45 до 59 - четвертая
 * - если число не входит в диапазон, то вывести сообщение об этом
 *
 * Пример пример сообщения:
 * System.out.println("первая")
 *
 * Пример вызова функции в main:
 * checkTimePath(15)
 * checkTimePath(100)
 *
 * +++2) В функцию приходит время в виде миллисекунд.
 * Необходимо вывести в консолько сколько в этих миллисекундах
 * часов, минут и секунд. 1секунда = 1000 милиссекунд.
 *
 * Пример вывода:
 * int hours = ...
 * System.out.println("Часы: " + hours)
 */

public class Task4 {

    public static void checkTimePath(int number) {
        if (number >= 0 && number <= 14)
            System.out.println("Первая");
        else if (number >= 15 && number <= 29)
            System.out.println("Вторая");
        else if (number >= 30 && number <= 44)
            System.out.println("Третья");
        else if (number >= 45 && number <= 59)
            System.out.println("Четвертая");
        else
            System.out.println("Число не входит в диапазон");
    }

    public static void parseMilliseconds(long milliseconds) {
        int hours = (int) (milliseconds / 3600000);
        System.out.println("Часы: " + hours);
        int min = (int) ((milliseconds % 3600000) / 60000);
        System.out.println("Минуты: " + min);
        int sec = (int) (((milliseconds % 3600000) % 60000) / 1000);
        System.out.println("Секунды: " + sec);
        int milisec = (int) (((milliseconds % 3600000) % 60000) % 1000);
        System.out.println("Милисекунды: " + milisec);
    }

    public static void main(String[] args) {
        
        checkTimePath(15);
        checkTimePath(100);

        System.out.println();

        parseMilliseconds (698752);
    }
}
