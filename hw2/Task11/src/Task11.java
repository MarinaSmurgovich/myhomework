/*
 * 11)	Есть массив чисел с плавающей запятой.
 * Необходимо реализовать сортировку пузырьком
 *  по возрастанию и по убыванию; */

public class Task11 {
    public static void main(String[] args) {
        double[] array = {4.7, -0.58, 6.45, 5.5, -10.03, -3.09, 8.0};

        for (int j = 0; j < array.length; j++) {
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] < array[i + 1]) {
                    double temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
        for (int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
    }
}


