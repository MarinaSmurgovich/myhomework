/* 2)	Посчитать степень числа.
Использовать цикл for;*/

public class Task2 {
    public static void main(String[] args) {
        int a = 2;
        /** NOTE! Эта инициализация избыточна.
         * Ты все равно присваиваешь начальное значение n в цикле.
         * Так что можно просто приписать int твоей переменной
         * в блоке инициализации в цикле
         */
        //int n = 6;
        int rez = 1;
        for (int n = 1; n <= 6; n++) {
            rez *= a;
        }
        System.out.println(rez);
    }
}
