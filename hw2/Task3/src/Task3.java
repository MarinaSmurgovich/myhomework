/* 3)	Дано целое число от 1 – 100.
* Необходимо найти максимальный делить числа (кроме 1 и самого числа),
* деление на которое не дает остаток (т.е. деление без остатка);
*/
public class Task3 {
    public static void main (String [] args) {
        int number = 95;
        int del = 0;
        /** NOTE! Я бы пробегался в цикле до значения number
         * Все равно если i будет больше чем number, то
         * целого числа уже не будет
         */
//        for (int i = 1; i <= 100; i++) {
        for (int i = 1; i <= number; i++) {
            if (number % i == 0 && i != number)
                del = i;
        }
        System.out.println(del);
    }
}
