/* 4)	Есть массив из десяти целых чисел.
 Необходимо вывести максимально и минимальное число из массива
 */

public class Task4 {
    public static void main(String[] args) {

        /** NOTE! Это задание можно сделать с использование только
         * одного цикла.
         */

        int[] array = {1, 25, 8, -12, 59, 64, -34, 5, 8, 9};
        int max = array[0];
        int min = array[0];

//        for (int i = 0; i < array.length; i++) {
//            if (array[i] > max)
//                max = array[i];
//        }
//        System.out.println("Максимальное число из массива: " + max);
//
//        for (int i = 0; i < array.length; i++) {
//            if (array[i] < min)
//                min = array[i];
//        }
//        System.out.println("Минимальное число из массива: " + min);
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max)
                max = array[i];
            if (array[i] < min)
                min = array[i];
        }
        System.out.println("Максимальное число из массива: " + max);
        System.out.println("Минимальное число из массива: " + min);
    }
}
