/*5)	Есть массив из десяти целых чисел.
 Необходимо поменять элементы массива в обратном порядке;*/

public class Task5 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int temp;
        int k = array.length;
        for (int i = 0; i < (array.length / 2); i++) {
            k -= 1;
            temp = array[i];
            array[i] = array[k];
            array[k] = temp;
        }
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
