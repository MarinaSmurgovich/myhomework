/* 8)	Есть массив из десяти целых чисел.
 Необходимо посчитать среднее арифметическое для всех элементов массива
 */

public class Task8 {
    public static void main(String[] args) {
//        int size = 10;
//        int[] array = new int[size];
        int[] array = new int[10];
        double sum = 0;
//        double result;
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10 + 1);
            System.out.print(array[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }

        /** NOTE!
         *
         * 1) Вместо size я бы использовал array.length.
         * Это более гибкое решение, чем использовать некую переменную.
         *
         * 2) я бы перенес инициализацию переменной result сюда.
         * От нее нет смысла перед циклом.(строка 10)
         */
        double result = sum / array.length;
        System.out.println("Среднее арифметическое для всех элементов массива: " + result);
    }
}
