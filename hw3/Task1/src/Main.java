
public class Main {
    /**
     * NOTE! Параметры a и b, и методы для арифметических операций
     * можно вынести в отдельный класс. Тогда по сути у тебя будет класс,
     * который будет работать с консолью и класс-калькулятор.
     * А тут просто как поле коасса у тебя будет объект калькулятора
     */

    public static void main(String[] args) {
        Calk calk = new Calk();

//        Calk addResult = new Calk();
//        Calk subtractionResult = new Calk();
//        Calk multiplicationResult = new Calk();
//        Calk divisionResult = new Calk();
//
        int number;
        int num1;
        int num2;
        int num3;

        do {
            System.out.println("«Выберите функцию»");
            System.out.println("1: Сложение");
            System.out.println("2: Разность");
            System.out.println("3: Умножение");
            System.out.println("4: Деление");
            System.out.println("0: Выход");
            System.out.println();
            System.out.print("Ваш выбор: ");

            number = Scaner.scaner();

            if (number == 1) {
                /**
                 * NOTE! У тебя много однотипного кода, который раскидан по
                 * всему класса, например, считывание данных из консоли.
                 * Это можно вынести в отдельный метод и просто вызывать его там, где
                 * тебе надо.
                 */
                num1 = Scaner.scaner1();
                num2 = Scaner.scaner2();
                calk.subtraction(num1, num2);
                System.out.println();

            } else if (number == 2) {
                num1 = Scaner.scaner1();
                num2 = Scaner.scaner2();
                calk.subtraction(num1, num2);

                System.out.println();


            } else if (number == 3) {
                num1 = Scaner.scaner1();
                num2 = Scaner.scaner2();
                calk.multiplication(num1, num2);

                System.out.println();

            } else if (number == 4) {
                num1 = Scaner.scaner1();
                num2 = Scaner.scaner2();

                if (num2 != 0) {
                    calk.division(num1, num2);

                    System.out.println();

                } else {
                    System.out.println("Делить на ноль нельзя!!!!");
                    System.out.println();

                    System.out.println("1: Ввести другое число");
                    System.out.println("2: Выйти в меню. ");
                    System.out.println();
                    System.out.print("Ваш выбор:");
                    num3 = Scaner.scaner();
                    System.out.println();

                    if (num3 == 1){
                        num2 = Scaner.scaner2();
                        calk.division(num1, num2);
                        System.out.println();
                    }
                }
            }
        }
        while (number != 0);
    }
}

