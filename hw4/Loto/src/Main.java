public class Main {
    public static void main(String[] args) {
        int number = ParticipantFactory.numberOfParticipants();
        Participant[] participants = ParticipantFactory.madeParticipants(number);
        int n = Participant.numberOfParticipant();
        int[] array = GameSimulator.made();
        System.out.println();
        System.out.print("Участники, которые выиграли: ");
        for (int i = 0; i < participants.length; i++) {
            int count = 0;
            for (int j = 0; j < participants[i].getArrayForParticipant().length; j++) {
                for (int k = 0; k < array.length; k++) {
                    if (participants[i].getArrayForParticipant()[j] == array[k]) {
                        count += 1;
                    }
                }
            }
            if (count == 3)
                System.out.print((i + 1) + " ");
            if (count == 3 && i + 1 == n)
                System.out.println("и Вы победили!");
        }
    }
}
