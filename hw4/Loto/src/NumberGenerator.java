import java.util.Random;

public class NumberGenerator {
    private int size;

    private NumberGenerator(int size) {
        this.size = size;
    }

    public static int[] randomArray(int size) {
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            Random random = new Random();
            array[i] = random.nextInt(49);
            System.out.print(array[i] + " ");
        }
        return array;
    }
}
