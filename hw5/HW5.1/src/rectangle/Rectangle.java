package rectangle;

public class Rectangle extends Square {
    protected double height;

    public Rectangle(String name, double width, double height) {
        super(name, width);
        this.height = height;
    }

    public void sayYourName () {
        System.out.println("Название фигуры - " + getName());
    }

    @Override
    public double area() {
        double area = width * height;
        System.out.println(area);
        return width * height;
    }
    @Override
    public void perimeter () {
        System.out.println("Периметр равен: " + ((width * 2) + (height * 2)));
    }
}
