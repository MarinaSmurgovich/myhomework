package rectangle;

public class Square extends TwoShape {
    protected double width;

    public Square(String name, double width) {
        super(name);
        this.width = width;
    }

    public void sayYourName () {
        System.out.println("Название фигуры - " + getName());
    }

    public double area() {
        double area = width * width;
        System.out.println(area);
        return width * width;
    }
    public void perimeter () {
        System.out.println("Периметр равен: " + width * 4);

    }

}

