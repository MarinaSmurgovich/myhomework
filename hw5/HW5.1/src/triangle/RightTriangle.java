package triangle;

public class RightTriangle extends IsoscelesRightTriangle {
    protected double twoCathet;

    public RightTriangle(String name, double width, double hypotenuse, double twoCathet) {
        super(name, width, hypotenuse);
        this.twoCathet = twoCathet;
    }

    /** NOTE! Нет необходимости переопередять этот метод.
     * Так как ты все равно вызываешь только метод супер-класса.
     */
    public void sayYourName() {
        super.sayYourName();
    }

    @Override
    public double area() {
        double area = 0.5 * width * twoCathet;
        System.out.println(area);
        return 0.5 * width * twoCathet;
    }

    @Override
    public void perimeter() {
        System.out.println("Периметр равен: " + (width + hypotenuse + twoCathet));
    }
}
