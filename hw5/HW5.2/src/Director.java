public class Director extends ProjectManager {
    public Director(String fullName, int age, String position, String duties, int salary, String responsibility) {
        super(fullName, age, position, duties, salary, responsibility);
    }

    /** NOTE! Нет необходимости переопередять этот метод.
     * Так как ты все равно вызываешь только метод супер-класса.
     */
    public void introduceYourself() {
        super.introduceYourself();
    }

    @Override
    public void doJob(int day) {
        System.out.println("Ответственность за " + getResponsibility());
        System.out.println("Обязанности: " + getDuties());
        System.out.println("Зарплата за месяц: " + getSalary() * day);
    }
}
