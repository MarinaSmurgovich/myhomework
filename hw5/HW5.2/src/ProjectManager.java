public class ProjectManager extends LeadingSpecialist {

    public ProjectManager(String fullName, int age, String position, String duties, int salary, String responsibility) {
        super(fullName, age, position, duties, salary, responsibility);
    }

    /** NOTE! Нет необходимости переопередять этот метод.
     * Так как ты все равно вызываешь только метод супер-класса.
     */
    public void introduceYourself() {
        super.introduceYourself();
    }

    public void doJob(int day, int dayForTrainees) {
        super.doJob(day);
        System.out.println("Оплата за обученик стажеров: " + getSalary() / 2 * dayForTrainees + ". Итого за месяц: " +  ((getSalary() / 2 * dayForTrainees) + (getSalary() * day)));
    }
}
