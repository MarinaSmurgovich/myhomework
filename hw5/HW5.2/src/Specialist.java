public class Specialist extends Trainee {
    private String duties;
    private int salary;

    public Specialist(String fullName, int age, String position, String duties, int salary) {
        super(fullName, age, position);
        this.duties = duties;
        this.salary = salary;

    }

    public String getDuties() {
        return duties;
    }

    public void setDuties(String duties) {
        this.duties = duties;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    /** NOTE! Нет необходимости переопередять этот метод.
     * Так как ты все равно вызываешь только метод супер-класса.
     */
    public void introduceYourself() {
        super.introduceYourself();
    }

    public void doJob(int day) {
        System.out.println("Обязанности: " + getDuties());
        System.out.println("Зарплата за месяц: " + salary * day);
    }
}
