public class Trainee extends Person {
    private String position;

    public Trainee(String fullName, int age, String position) {
        super(fullName, age);
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public void introduceYourself() {
        super.introduceYourself();
        System.out.println("Должность: " + getPosition());
    }

    public void doJob (){
        System.out.println("Обучается");
    }
}
