public class Car {
    private String title;
    private int year;
    private String engine;
    private int engineVolume;

    public Car(String title, int year, String engine, int engineVolume) {
        this.title = title;
        this.year = year;
        this.engine = engine;
        this.engineVolume = engineVolume;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public int getEngineVolume() {
        return engineVolume;
    }

    public void setEngineVolume(int engineVolume) {
        this.engineVolume = engineVolume;
    }

    public void introduceYourSelf () {
        System.out.println();
    }
}
