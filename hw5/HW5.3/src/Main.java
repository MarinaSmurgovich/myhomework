import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Trucks[] trucks = {
                new Trucks("Volvo FH12", 2015, "дизель", 12100, 26),
                new Trucks("Mercedes Actros 1936", 2016, "дизель", 11000, 13.5),
                new Trucks("DAF XF 105 460", 2012, "дизель", 12000, 20),
                new Trucks("MAN TGL 7.150", 2008, "дизель", 4580, 6.1),
                new Trucks("Mercedes Sprinter", 2009, "дизель", 2200, 2)
        };

        PassengerCar[] passengerCars = {
                new PassengerCar("Audi A6", 2018, "бензин", 1800, 5),
                new PassengerCar("Mazda Mazda3", 2019, "бензин", 1500, 5),
                new PassengerCar("BMW X5", 2019, "дизель", 3000, 5),
                new PassengerCar("Audi TT", 2001, "бензин", 1800, 2),
                new PassengerCar("Lexus RX350", 2015, "бензин", 3500, 5)
        };

        System.out.println("Выберите тип автомобиля:");
        System.out.println("1 - грузовые");
        System.out.println("2 - легковые");
        System.out.println("Выбор: ");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();

        /** NOTE! По идее можно просто закинуть все объекты в массив и
         * печатать их через цикл.
         *

         Trucks[] trucks = {
            new Trucks("DAF XF 105 460", 2012, "дизель", 12000, 20),
            new Trucks("MAN TGL 7.150", 2008, "дизель", 4580, 6.1),
            new Trucks("Mercedes Sprinter", 2009, "дизель", 2200, 2)
         }

         * */
        switch (number) {
            case 1:
                for (int i = 0; i < trucks.length; i++){
                    trucks[i].introduceYourSelf();
                }
                break;
            case 2:
                for (int i = 0; i < passengerCars.length; i++) {
                    passengerCars[i].introduceYourSelf();
                }
                break;
            default:
                System.out.println("Введено неверное число");
        }
    }
}
