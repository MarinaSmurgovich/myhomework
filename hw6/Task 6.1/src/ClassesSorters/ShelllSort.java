package ClassesSorters;

public class ShelllSort extends Sortable {


    public static void shellSort1(int[] array) {
        int d = 10;
        d = d / 2;
        while (d > 0) {
            for (int i = 0; i < 10 - d; i++) {
                int j = i;
                while (j >= 0 && array[j] > array[j + d]) {
                    int temp = array[j];
                    array[j] = array[j + d];
                    array[j + d] = temp;
                    j--;
                }
            }
            d = d / 2;
        }
        for (int i = 0; i < 10; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public static void shellSort2(int[] array) {
        int d = 10;
        d = d / 2;
        while (d > 0) {
            for (int i = 0; i < 10 - d; i++) {
                int j = i;
                while (j >= 0 && array[j] < array[j + d]) {
                    int temp = array[j];
                    array[j] = array[j + d];
                    array[j + d] = temp;
                    j--;
                }
            }
            d = d / 2;
        }
        for (int i = 0; i < 10; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
