import ClassesSorters.BubbleSort;
import ClassesSorters.SelectionSort;
import ClassesSorters.ShelllSort;
import ClassesSorters.Sortable;
import Scaners.Scaner;
import Scaners.Scaner1;

public class Main {
    public static void main(String[] args) {
        int array[] = Sortable.getArray();
        System.out.println("Меню:");
        System.out.println("1 - Сортировка пузырьком");
        System.out.println("2 - Сортировка методом выбора");
        System.out.println("3 – Сортировка методом Шелла");
        System.out.println("0 – Выход");
        switch (Scaner.getNumber()) {
            case 1:
                switch (Scaner1.getNumber()) {
                    case 1:
                        BubbleSort.bubbleSortUp(array);
                        break;
                    case 2:
                        BubbleSort.bubbleSort(array);
                        break;
                }
                break;
            case 2:
                switch (Scaner1.getNumber()) {
                    case 1:
                        SelectionSort.selectionSort1(array);
                        break;
                    case 2:
                        SelectionSort.selectionSort2(array);
                        break;
                }
                break;
            case 3:
                switch (Scaner1.getNumber()) {
                    case 1:
                        ShelllSort.shellSort1(array);
                        break;
                    case 2:ShelllSort.shellSort2(array);
                        break;
                }
                break;
            case 0:
                break;
            default:
                System.out.println("Введено неверное число");
        }
    }
}
