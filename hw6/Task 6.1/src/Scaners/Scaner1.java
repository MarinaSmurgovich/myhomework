
package Scaners;

public class Scaner1 {
    private int number;

    public Scaner1(int number) {
        this.number = number;
    }

    public static int getNumber() {
        System.out.println("1 - по возрастанию");
        System.out.println("2 - по убыванию");
        java.util.Scanner in = new java.util.Scanner(System.in);
        int number = in.nextInt();
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
