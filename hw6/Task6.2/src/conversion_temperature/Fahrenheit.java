package conversion_temperature;

public class Fahrenheit extends Temperature {

    public Fahrenheit() {
    }

    @Override
    public void conversion(double t) {
        //(0 °C × 9/5) + 32 = 32 °F
        double result = (t * 9 / 5) + 32;
        System.out.println("Результат конвертации: " + result);
    }
}
