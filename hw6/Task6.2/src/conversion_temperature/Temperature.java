package conversion_temperature;

public abstract class Temperature {
    private double t;

    public Temperature() {

    }

    public abstract void conversion(double t);
}
