package Printers;

public class AdvertisingDisplay implements Printer {
    @Override
    public void introduceYourSelf() {
        System.out.println("Рекламный дисплей");
    }

    @Override
    public void print(String message) {
        System.out.println(message);

    }
}
