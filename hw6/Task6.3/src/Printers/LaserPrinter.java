package Printers;

public class LaserPrinter implements Printer {
    @Override
    public void introduceYourSelf() {
        System.out.println("Лазерный принтер");
    }

    @Override
    public void print(String message) {
        System.out.println(message);
    }
}
