package Printers;

interface Printer {
    public void introduceYourSelf ();

    public void print (String message);
}
