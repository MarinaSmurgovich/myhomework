package Printers;

public class Printer3D implements Printer {
    @Override
    public void introduceYourSelf() {
        System.out.println("3D-принтер");
    }

    @Override
    public void print(String message) {
        System.out.println(message);

    }
}
