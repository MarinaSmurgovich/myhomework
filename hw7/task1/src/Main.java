public class Main {
    public static class Recursion {

        public String strRecursion(int n) {
            if (n == 1) {
                return "1";
            }
            return strRecursion(n - 1) + " " + n;
        }
    }

    public static void main(String[] args) {
        Recursion recursion = new Recursion();
        System.out.print(recursion.strRecursion(100));
    }
}
