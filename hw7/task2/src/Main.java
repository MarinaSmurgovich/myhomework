public class Main {
    public static int numberRecursion (int n) {
        if ( n < 10) {
            return n;
        }
        else {
           System.out.print(n % 10);
           return numberRecursion(n / 10);
        }
    }
    public static void main(String[] args) {
        System.out.println(numberRecursion(123456789));

    }
}
