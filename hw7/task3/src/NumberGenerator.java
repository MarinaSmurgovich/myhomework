import java.util.ArrayList;
import java.util.Random;
import java.util.TreeSet;

public class NumberGenerator {
    private int size;

    private NumberGenerator(int size) {
        this.size = size;
    }

    public static ArrayList<Integer> random(int size) {
        ArrayList<Integer> numbersR = new ArrayList<>();
        Random random = new Random();
        while (numbersR.size() < size) {
            int a = random.nextInt(49) + 1;
            if (!numbersR.contains(a)) {
                numbersR.add(a);
            }
        }
//        System.out.print(numbersR);
        return numbersR;
    }
}