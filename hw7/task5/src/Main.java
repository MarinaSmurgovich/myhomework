import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();
        list.add("Иванов Иван Иванович, директор, 50 лет");
        list.add("\n Викторов Виктор Викторович, руководитель проекта, 45 лет ");
        list.add("\n Степанов Спепан Степанович, специалист, 30 лет");
        list.add("\n Алексеев Алексей Алексеевич, стажер, 18 лет");

        System.out.println(list);
        System.out.println();

        list.add(1, "\n Васильев Василий Васильевич, заместиель директора, 45 лет");

        System.out.println("Обновленный список: \n" + list);

        System.out.println(list.get(3));
    }
}
