package menu;

public class MainMenuConsts {
    static final String ITEM_TEXT_READ_FILE = "Прочитать файл";
    static final String ITEM_ADD_FILE = "Добавить файл";
    static final String ITEM_DELETE_FILE = "Удалить файл";
    static final String ITEM_ADD_NEW_TEXT = "Добавить в файл новый текст";
    static final String ITEM_ADD_TO_ZIP = "Добавить файлы в архив";
    static final String ITEM_MOVING_TO_BACKUP = "Переместить архив в папку \"BackUP\"";
    static final String ITEM_BACKUP_REPLACEMENT = "Восстановить файлы из последней версии резервной копии";
    static final String ITEM_TEXT_EXIT = "Выход";
    public static final String ITEM_WiTH_NUMBER = "%d - %s";
    public static final String READ_FILE_ERROR_MESSAGE = "Файл не прочитан";
    public static final String WRITE_FILE_ERROR_MESSAGE = "Файл не записан";
    public static final String ACTION_ITEM_ERROR_MESSAGE = "Пункт меню не распознан";

    private MainMenuConsts () {

    }

}
