package menu;

import menu.item.MainMenuAction;
import menu.item.MenuItem;

import java.util.LinkedList;
import java.util.List;

import static menu.MainMenuConsts.*;

public class MenuFactory {

    public static List<MenuItem> getMainMenu (MainMenuAction... menuActions) {
        LinkedList<MenuItem> linkedList = new LinkedList<MenuItem>();
        for (int i = 0; i < menuActions.length; i++){
            MenuItem menuItem = getMainMenuItem(i, menuActions[i]);
            if (menuItem != null) {
                linkedList.add(menuItem);
            }
        }
        return linkedList;
    }

    private static MenuItem getMainMenuItem (int ind, MainMenuAction mainMenuAction) {

        switch (mainMenuAction) {
            case REED_FILE: return new MenuItem(ind, ITEM_TEXT_READ_FILE);
            case ADD_FILE: return new MenuItem(ind, MainMenuConsts.ITEM_ADD_FILE);
            case DELETE_FILE: return new MenuItem(ind, ITEM_DELETE_FILE);
            case ADD_NEW_TEXT: return new MenuItem(ind, ITEM_ADD_NEW_TEXT);
            case ADD_TO_ZIP: return new MenuItem(ind, ITEM_ADD_TO_ZIP);
            case MOVING_TO_BACKUP: return new MenuItem(ind, ITEM_MOVING_TO_BACKUP);
            case BACKUP_REPLACEMENT: return new MenuItem(ind, ITEM_BACKUP_REPLACEMENT);
            case EXIT: return new MenuItem(ind, ITEM_TEXT_EXIT);
        }
            return null;
    }

    private MenuFactory () {

    }
}
