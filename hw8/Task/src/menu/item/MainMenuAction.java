package menu.item;

import exceptions.MenuActionException;

public enum MainMenuAction {
    REED_FILE, ADD_FILE, DELETE_FILE, ADD_NEW_TEXT, ADD_TO_ZIP,MOVING_TO_BACKUP,BACKUP_REPLACEMENT, EXIT;

    public static MainMenuAction getMainMenuAction (int actionNumber) throws MenuActionException{
        MainMenuAction[] valuesArray = values();
        if (actionNumber >= 0 && actionNumber < values().length) {
            return valuesArray[actionNumber];
        }
        throw new MenuActionException("Wrong action number");
    }

}
